/**
 * Created by Дмитрий on 26.11.2017.
 */
export const NEW_TAB = 'NEW_TAB';
export const CHOOSE_CITY = 'CHOOSE_CITY';
export const LOAD_FORECAST = 'LOAD_FORECAST';
export const LOAD_FORECAST_SUCCESS = 'LOAD_FORECAST_SUCCESS';
export const SAVE_LAST_QUERY = 'SAVE_LAST_QUERY';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';

