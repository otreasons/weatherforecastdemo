/**
 * Created by Дмитрий on 26.11.2017.
 */
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import mainReducer from './reducers';

const rootReducer = combineReducers({
    mainReducer,
    form: formReducer
});

export default rootReducer;