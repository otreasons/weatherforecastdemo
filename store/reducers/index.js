/**
 * Created by Дмитрий on 27.11.2017.
 */
import { NEW_TAB,
         CHOOSE_CITY,
         LOAD_FORECAST,
         LOAD_FORECAST_SUCCESS,
         SAVE_LAST_QUERY,
         TOGGLE_MODAL
} from '../constants';

const initialState = {
    tabs: [],
    cities: [
        {woeid: 2346910, name: 'Москва'},
        {woeid: 20070507, name: 'Санкт-Петербург'},
        {woeid: 615702, name: 'Париж'},
        {woeid: 2459115, name: 'Нью-Йорк'},
        {woeid: 638242, name: 'Берлин'},
        {woeid: 721943, name: 'Рим'},
        {woeid: 560743, name: 'Дублин' },
        {woeid: 796597, name: 'Прага'},
        {woeid: 523920, name: 'Варшава'},
        {woeid: 766273, name: 'Мадрид'}
    ],
    isOpen: false,
    lastQuery: {}
};

export default function mainReducer(state = initialState, action) {
    switch(action.type) {
        case NEW_TAB:
             return {
                 ...state,
                 tabs: state.tabs.concat({...action.data, tab_id: state.tabs.length})
             };
        case CHOOSE_CITY:
            return {
                ...state,
                tabs: state.tabs.map((tab) => {
                   if (tab.tab_id === action.tab_id) {
                       return {
                           ...tab,
                           tab_title: action.city
                       }
                   }
                   return tab;
                })
            };
        case LOAD_FORECAST:
            return {
                ...state,
                fetching: action.fetching
            };
        case LOAD_FORECAST_SUCCESS:
            return {
                ...state,
                tabs: state.tabs.map((tab) => {
                    if (tab.tab_id === action.tab_id) {
                        return {
                            ...tab,
                            forecast: action.forecast
                        }
                    }
                    return tab;
                })
            };
        case SAVE_LAST_QUERY:
            return {
                ...state,
                lastQuery: {
                    query: action.lastQuery.query,
                    res: action.lastQuery.res
                }
            };
        case TOGGLE_MODAL:
            return {
                ...state,
                isOpen: !state.isOpen
            };
        default:
            return state;
    }
}