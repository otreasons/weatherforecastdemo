/**
 * Created by Дмитрий on 26.11.2017.
 */
import { NEW_TAB,
    CHOOSE_CITY,
    LOAD_FORECAST,
    LOAD_FORECAST_SUCCESS,
    SAVE_LAST_QUERY,
    TOGGLE_MODAL
} from '../constants';
import fetch from '../../utils/fetch';


export function newTab() {
    return dispatch => dispatch({
        type: NEW_TAB,
        data: {
            tab_title: 'New tab'
        }
    });
}

export function toggleModal() {
    return {
        type: TOGGLE_MODAL
    }
}

export function loadForecast(form) {
       const props = Array.prototype.slice.call(arguments, 2)[0];
       const _id = props._id;
       const query = 'select item.forecast from weather.forecast where woeid=' + form.woeid;
       const city_name = props.cities.filter((city) => {
           if (city.woeid == form.woeid) {
               return city.name;
           }
       });

       return dispatch => {
            dispatch({
                type: LOAD_FORECAST,
                fetching: true
            });

            return fetch('http://localhost:3000/services/getWeather', {
                method: 'POST', body: JSON.stringify({ query })
            })
                .then((res) => {
                console.log(res);
                    dispatch({ type: CHOOSE_CITY, city: city_name[0].name, tab_id: _id});
                    dispatch({ type: LOAD_FORECAST_SUCCESS, forecast: res, tab_id: _id});
                    dispatch({ type: SAVE_LAST_QUERY, lastQuery: { query, res } });
                })

       }
}