/**
 * Created by Дмитрий on 21.11.2017.
 */
const express = require('express'),
      path = require('path'),
      app = express(),
      bodyParser = require('body-parser'),
      YQL = require('yql'),
      q = require('q'),
      processResponse = require('../utils/processResponse'),
      port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.use('/spa', express.static(path.join(__dirname, '../dist')));

app.get("*", function (req, res) {
   res.sendFile(path.join(__dirname, '../public/index.html'));
});

app.post('/services/getWeather', function (req, res) {
   let query = new YQL(req.body.query);


    query.exec((err, data) => {
            res.json(processResponse(data.query.results.channel));
    });
    // query.exec(function(err, data) {
    //   if (err) {
    //       console.log(err);
    //   } else {
    //       res.json(data.query.results);
    //   }
    // });
});

app.listen(port, function () {
   console.log('WeatherApp is runnning on port № ' + port);
});