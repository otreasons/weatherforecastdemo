import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import WeatherApp from '../containers/WeatherApp';
import store from '../store';
import './styles.css';

function App() {
    console.log(store.getState());
    return (
        <Provider store={store}>
            <WeatherApp />
        </Provider>
    )
}


ReactDOM.render(
    <App />,
    document.getElementById('root')
);