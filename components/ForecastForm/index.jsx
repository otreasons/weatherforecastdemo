import React from 'react';
import { Field, reduxForm } from 'redux-form';

export default function ForecastForm(props) {
    const { handleSubmit } = props;
    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="woeid">
                <Field name="woeid" component="select">
                    {
                        props.cities.map((city, i) => {
                            return <option key={i} value={city.woeid}>{city.name}</option>
                        })
                    }
                </Field>
            </label>
            <button className="submitButton" type="submit">Загрузить прогноз</button>
        </form>
    )
}

ForecastForm = reduxForm({
    initialValues: { woeid: '2346910' },
    form: 'city' // a unique name for this form
})(ForecastForm);