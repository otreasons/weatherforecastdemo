/**
 * Created by Дмитрий on 26.11.2017.
 */
import React from 'react';

export default function UserMenu(props) {
    return (
        <div className="menuContainer">
            <button className="button" onClick={ () => props.newTab() } >Новая вкладка</button>
            <button className="button" onClick={ () => props.toggleModal() } >Показать последний запрос</button>
        </div>
    )
}