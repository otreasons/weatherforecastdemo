import React from 'react';
import { TabPanel } from 'react-tabs'
import ForecastTable from '../ForecastTable';
import ForecastForm from '../ForecastForm';

export default function ForecastTab(props) {
    return (
        <div>
            <ForecastTable />
            <ForecastForm />
        </div>
    );
}