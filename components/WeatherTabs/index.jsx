import React from 'react';
import { Tabs, Tab, TabList, TabPanel } from 'react-tabs';
import ForecastTable from '../ForecastTable';
import ForecastForm from '../ForecastForm';


const styles = {
    tabsContainer: {
        float: 'right',
        width: '85%',
        height: '100vh',
        position: 'realative',
        background: '#EEE',
        zIndex: '0'
    },
    tabList: {
        listStyleType: 'none'
    },
    tab: {
        display: 'inline',
        marginRight: '5px'
    }
}

export default function WeatherTabs(props) {
    return (
        <div className="tabsContainer">

            <Tabs>
                <TabList className="tabList">
                    {
                      props.tabs.map((tab) => <Tab className="tab">{ tab.tab_title }</Tab> )
                    }
                </TabList>
                {
                    props.tabs.map((tab) => {
                        return (
                            <TabPanel className="tabPanel">
                                <ForecastTable data={tab.forecast}/>
                                <ForecastForm
                                    _id={tab.tab_id}
                                    cities={props.cities}
                                    onSubmit={props.loadForecast}
                                />
                            </TabPanel>
                        )
                    } )
                }
            </Tabs>
        </div>
    )
}