/**
 * Created by Дмитрий on 01.12.2017.
 */
import React from 'react';
import Modal from 'react-modal';

const style= {
    content: {
        left: '250px',
        right: '250px',
        top: '150px',
        bottom: '150px'
    },
    overlay: {
        background: 'rgba(0, 0, 0, 0.75)'
    }
};

export default function LastQueryModal(props) {
    return (
        <Modal
            isOpen={props.isOpen}
            style={style}
        >
            <div>
                <label htmlFor="queryString" style={{display: 'block', marginBottom: '10px', width: '100%'}}>
                    Запрос:
                    <input style={{width: '100%'}} id="queryString" type="text" defaultValue={props.lastQuery.query}/>
                </label>
                <label htmlFor="response" style={{display: 'block', width: '100%'}}>
                    Ответ от Yahoo Weather API:
                    <input style={{width: '100%'}} id="response" type="text" defaultValue={props.lastQuery.res}/>
                </label>
                <button onClick={ () => props.toggleModal() } className="closeModal">Закрыть</button>
            </div>
        </Modal>
    );
};
