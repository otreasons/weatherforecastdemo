import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

const columns = [{
    Header: 'Дата',
    accessor: 'date',
    Cell: date => <div style={{textAlign: 'center'}}>{ date.value }</div>
}, {
    Header: 'День недели',
    accessor: 'day',
    Cell: day => <div className={ day.value === 'Sat' || day.value === 'Sun' ? 'weekend' : 'day'}>{day.value}</div>
}, {
    Header: 'Температура',
    accessor: 'temp',
    Cell: temp => <div style={{textAlign: 'center'}}>{ temp.value }</div>
}, {
    Header: 'Разница температуры',
    accessor: 'tempDiff',
    Cell: tempDiff => {
        let className;
        if (tempDiff.value) {
           className = tempDiff.value > 0 ? 'warmer' : 'colder';
        } else {
            className = 'sameTemp'
        }
        return <div className={className}>{ tempDiff.value }</div> }
}
];

export default function ForecastTable(props) {
    return (
        <ReactTable
            sortable={false}
            showPagination={false}
            data={props.data}
            columns={columns}
            minRows={5}
        />
    )
};
