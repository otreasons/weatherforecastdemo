/**
 * Created by Дмитрий on 29.11.2017.
 */
import fetch from 'isomorphic-fetch';

export default function fetchWrapper(url, options) {
    let headers = new Headers({
        'Content-Type': 'application/json',
    });

    return fetch(url, {...options, headers})
        .then((res) => {
            switch(res.status) {
                case 500: return Promise.reject(res);
                default: return res;
            }
        })
        .then((res) => res.json());
}
