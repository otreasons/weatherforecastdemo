module.exports = function fahrenheitToCelsius(t) {

    return ((t/2) - 32) * (5/9);
};
