/**
 * Created by Дмитрий on 30.11.2017.
 */
let fahrenheitToCelsius = require('../fahrenheitToCelsius');

module.exports = function processResponse(forecast) {

    let processedForecast = forecast.map((day, i, arr) => {
        let item = day.item.forecast;


        return {
            date: item.date,
            day: item.day,
            temp: Math.round(fahrenheitToCelsius(parseInt(item.high) + parseInt(item.low))),
        };


    });

    for (let i = 0; i <= processedForecast.length-1; i++) {
        if (processedForecast[i-1] !== undefined) {
            processedForecast[i].tempDiff = processedForecast[i].temp - processedForecast[i-1].temp;
        }
    }

    return processedForecast;
}