/**
 * Created by Дмитрий on 21.11.2017.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { newTab, loadForecast, toggleModal } from '../../store/actions';
import UserMenu from '../../components/UserMenu';
import WeatherTabs from '../../components/WeatherTabs';
import LastQueryModal from '../../components/LastQueryModal';

class WeatherApp extends Component {
    constructor(props) {
        super(props);

    }

    render () {
       return (
           <div>
                <UserMenu toggleModal={this.props.toggleModal} newTab={this.props.newTab}/>
                <WeatherTabs
                    loadForecast={ this.props.loadForecast }
                    tabs={ this.props.tabs }
                    cities={ this.props.cities }
                />
               <LastQueryModal
                   toggleModal={this.props.toggleModal}
                   lastQuery={this.props.lastQuery}
                   isOpen={this.props.isOpen}
               />
           </div>
       )
    };
};

function mapStateToProps(state) {
    return {
        tabs: state.mainReducer.tabs,
        tab_id: state.mainReducer.tabs.length,
        cities: state.mainReducer.cities,
        isOpen: state.mainReducer.isOpen,
        lastQuery: state.mainReducer.lastQuery
    }
}

function mapDispatchToProps(dispatch) {
    return  {
        newTab: bindActionCreators(newTab, dispatch),
        loadForecast: bindActionCreators(loadForecast, dispatch),
        toggleModal: bindActionCreators(toggleModal, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherApp);
